package com.tikhomirov.kanimdsl

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.TimeInterpolator
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Group

abstract class  Animation<T:Number> {
    lateinit var target: View
    lateinit var values: List<T>
    var duration: Long = 0L
    var startDelay: Long = 0L
    var repeatCount = 0
    var repeatMode: Int = 0
    var interpolator: TimeInterpolator? = null

    open var onStart:  (() -> Unit)? = null
    open var onEnd:    (() -> Unit)? = null
    open var onCancel: (() -> Unit)? = null
    open var onRepeat: (() -> Unit)? = null

    open fun getAnimator(): Animator {
        val animator = createAnimation()
        animator.apply {
            duration = this@Animation.duration
            startDelay = this@Animation.startDelay
            this@Animation.interpolator?.let {
                interpolator = this@Animation.interpolator
            }
        }
        animator.setCallbacks()
        return animator
    }

    protected open fun Animator.setCallbacks() {
        val hasCallbacks =
                    onStart  != null ||
                    onEnd    != null ||
                    onCancel != null ||
                    onRepeat != null

        if (hasCallbacks) {
            val animationListener = AnimationCallbackListener(
                onStart = onStart,
                onEnd = onEnd,
                onCancel = onCancel,
                onRepeat = onRepeat
            )
            this.addListener(animationListener)
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <S : Animation<T>> launchOnView(view: View, lambda: S.() -> Unit) {
        target = view
        (this as S).apply(lambda)
        val animator = getAnimator()
        animator.start()
    }

    @Suppress("UNCHECKED_CAST")
    fun <S : Animation<T>> createOnView(view: View, lambda: S.() -> Unit):Animator {
        target = view
        (this as S).apply(lambda)
        return getAnimator()
    }

    @Suppress("UNCHECKED_CAST")
    fun <S : Animation<T>> createOnGroup(group: Group, lambda: S.() -> Unit):Animator {
        val listOfAnimators = mutableListOf<Animator>()
        (this as S).apply(lambda)
        group.referencedIds.forEach {viewId->
            target = (group.parent as ViewGroup).findViewById<View>(viewId)
            listOfAnimators.add(0, getAnimator())
        }
        val set = AnimatorSet()
        set.playTogether(*listOfAnimators.toTypedArray())
        return set
    }

    @Suppress("UNCHECKED_CAST")
    fun <S : Animation<T>> launchOnGroup(group: Group, lambda: S.() -> Unit) {
       createOnGroup(group,lambda).start()
    }

    protected abstract fun createAnimation(): Animator

}