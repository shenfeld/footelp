package com.tikhomirov.kanimdsl.defaultAnimations

import android.animation.Animator
import android.animation.ObjectAnimator
import com.tikhomirov.annotations.KAnimation
import com.tikhomirov.kanimdsl.Animation

@KAnimation("color")
class ColorAnimation: Animation<Int>() {
    lateinit var property: String
    override fun createAnimation(): Animator {

        return ObjectAnimator.ofArgb(target, property, *this@ColorAnimation.values.toIntArray())
            .apply {
                duration = this@ColorAnimation.duration
                startDelay = this@ColorAnimation.startDelay

            }
    }
}
