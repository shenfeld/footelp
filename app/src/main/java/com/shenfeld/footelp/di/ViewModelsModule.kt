package com.shenfeld.footelp.di

import com.shenfeld.footelp.app.fragments.allowance_camera.AllowanceCameraViewModel
import com.shenfeld.footelp.app.fragments.footelp_info.FootelpInfoViewModel
import com.shenfeld.footelp.app.fragments.history_screen.HistoryViewModel
import com.shenfeld.footelp.app.fragments.home.HomeViewModel
import com.shenfeld.footelp.app.main_activity.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module

@OptIn(KoinApiExtension::class)
val viewModelsModule = module {
    viewModel { MainViewModel() }
    viewModel { HomeViewModel() }
    viewModel { FootelpInfoViewModel() }
    viewModel { AllowanceCameraViewModel() }
    viewModel { HistoryViewModel() }
}
