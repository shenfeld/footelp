package com.shenfeld.footelp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.shenfeld.footelp.base.ColorProvider
import com.shenfeld.footelp.base.ContextColorProvider
import com.shenfeld.footelp.base.ResourcesManager
import com.shenfeld.footelp.base.navigation.AppRouter
import com.shenfeld.footelp.base.navigation.BaseResourcesManager
import com.shenfeld.footelp.base.navigation.ScreenList
import com.shenfeld.footelp.base.navigation.ScreenListImpl
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone

@OptIn(KoinApiExtension::class)
val commonModule = module {
    single { createGsonInstance() }
    single { AppRouter() }
    single { Cicerone.create(get<AppRouter>()) }
    single<BaseResourcesManager> { ResourcesManager(androidContext()) }
    single<ColorProvider> { ContextColorProvider(androidContext()) }
    single<ScreenList> { ScreenListImpl() }
}

private fun createGsonInstance(): Gson {
    return GsonBuilder().create()
}