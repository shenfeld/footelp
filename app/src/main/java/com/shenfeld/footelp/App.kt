package com.shenfeld.footelp

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.shenfeld.footelp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.threeten.bp.LocalDateTime
import timber.log.Timber

class App : Application() {
    private var lastActivityPaused: LocalDateTime? = null

    override fun onCreate() {
        super.onCreate()
        setupKoin()
        AndroidThreeTen.init(this)
        Timber.plant(Timber.DebugTree())
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@App)
            modules(
                commonModule,
                apiServicesModule,
                repositoriesModule,
                domainModule,
                viewModelsModule
            )
        }
    }

}