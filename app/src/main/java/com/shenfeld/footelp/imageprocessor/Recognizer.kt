package com.shenfeld.footelp.imageprocessor

import android.content.Context
import android.graphics.Bitmap
import androidx.annotation.StringRes
import com.shenfeld.footelp.R
import com.shenfeld.footelp.ml.ModelVgg16
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.common.TensorOperator
import org.tensorflow.lite.support.common.TensorProcessor
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import timber.log.Timber
import java.util.*
import kotlin.math.min


open class Recognizer(
    private val context: Context,
) {
    private var imageSizeX = 299
    private var imageSizeY = 299
    private val IMAGE_MEAN = 0f
    private val IMAGE_STD = 255f
    private val PROBABILITY_MEAN = 0.0f
    private val PROBABILITY_STD = 1.0f

suspend fun recognizeImage(img: Bitmap): List<Recognition> {
    return withContext(Dispatchers.Default) {
preprocessImage(img)?.let {
    val model = ModelVgg16.newInstance(context)
    val inputFeature = TensorBuffer.createFrom(it.tensorBuffer, DataType.FLOAT32)
    val outputs = model.process(inputFeature)
    val outputsFeature = outputs.outputFeature0AsTensorBuffer
    val probabilityProcessor = TensorProcessor.Builder().add(getPostprocessNormalizeOp()).build()
    listOf(
        Recognition(
            id = "Probability",
            title = R.string.home_fragment_result_of_analyze_text,
            confidence = probabilityProcessor.process(outputsFeature).floatArray[0],
        )
    )
} ?: emptyList()
    }
}

    private fun getLabelPath(): String {
        return "labels.txt"
    }

    private fun getRecognitions(labeledProbability: Map<String, Float>): List<Recognition> {
        val pq = PriorityQueue<Recognition>(2) { o1, o2 -> o1.confidence.compareTo(o2.confidence) }
        labeledProbability.forEach { (key, value) ->
            pq.add(
                Recognition(
                    id = key,
                    title = R.string.home_fragment_result_of_analyze_text,
                    confidence = value
                )
            )
        }

        return pq.toList()
    }

    private suspend fun preprocessImage(img: Bitmap): TensorImage? {
        val tensorImage: TensorImage = TensorImage.fromBitmap(img)
        val width: Int = img.width
        val height: Int = img.height
        val cropSize = min(width, height)
        return ImageProcessor.Builder()
            .add(ResizeWithCropOrPadOp(cropSize, cropSize))
            .add(ResizeOp(
                imageSizeX,
                imageSizeY,
                ResizeOp.ResizeMethod.NEAREST_NEIGHBOR
            ))
            .add(getPreprocessNormalizeOp())
            .build()
            .process(tensorImage)
    }

    private fun getPreprocessNormalizeOp(): TensorOperator {
        return NormalizeOp(IMAGE_MEAN, IMAGE_STD)
    }

    private fun getPostprocessNormalizeOp(): TensorOperator {
        return NormalizeOp(PROBABILITY_MEAN, PROBABILITY_STD)
    }

    data class Recognition(
        val id: String,
        @StringRes val title: Int,
        val confidence: Float,
    )
}