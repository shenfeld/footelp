package com.shenfeld.footelp.base

import androidx.lifecycle.ViewModel
import com.shenfeld.footelp.base.navigation.AppRouter
import com.shenfeld.footelp.base.navigation.BaseResourcesManager
import com.shenfeld.footelp.base.navigation.Screen
import com.shenfeld.footelp.base.navigation.ScreenList
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
open class BaseViewModel : ViewModel(), KoinComponent {
    protected val appRouter: AppRouter by inject()
    protected val screenList: ScreenList by inject()
    protected val resourcesManager: BaseResourcesManager by inject()

    open fun navBack() {
        appRouter.exit()
    }

    fun openBackendError(returnScreen: Screen? = null, retryScreen: Screen? = null) {
        //TODO 18/04/2021 add realisation
    }

    fun openBackendErrorForResult(returnScreen: Screen? = null, onRetryClicked: (() -> Unit)?) {
        //TODO 18/04/2021 add realisation
    }
}