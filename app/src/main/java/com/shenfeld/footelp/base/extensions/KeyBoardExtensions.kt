package com.shenfeld.footelp.base.extensions

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

fun EditText?.showKeyboard() {
    this ?: return
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun Activity.hideKeyboard() {
    val inm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    val view = this.currentFocus
    inm.hideSoftInputFromWindow(view?.windowToken, 0)
}

fun Activity.hideKeyboard(clearFocus: Boolean = true) {
    (currentFocus ?: window.decorView).hideKeyboard(clearFocus)
}

fun View?.hideKeyboard(clearFocus: Boolean = true) {
    this ?: return
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
    if (clearFocus) clearFocus()
}