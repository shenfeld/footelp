package com.shenfeld.footelp.base.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.shenfeld.footelp.base.utils.Event

// get event only once
inline fun <T> Event<T>.getOnce(block: (T) -> Unit) {
    block(getValue() ?: return)
}

inline fun <T> Event<T>.getOnceIf(bool: Boolean, block: (T) -> Unit) {
    if (bool) {
        block(getValue() ?: return)
    }
}

// send en value as an event, live data will handel this only once if
// used with observeEvent extension
fun <T> T.asEvent(): Event<T> {
    return Event(this)
}

// get event only once
inline fun <T> LiveData<Event<T>>.observeEvent(
    owner: LifecycleOwner,
    crossinline observer: (value: T) -> Unit
) {
    this.observe(owner, Observer {
        it.getOnce { value ->
            observer.invoke(value)
        }
    })
}