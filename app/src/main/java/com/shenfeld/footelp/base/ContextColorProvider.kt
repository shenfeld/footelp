package com.shenfeld.footelp.base

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat

class ContextColorProvider constructor(
    private val ctx: Context?
) : ColorProvider {
    companion object {
        private val DEFAULT_VALUE = Color.WHITE
    }

    override fun getColor(colorId: Int): Int {
        ctx ?: return DEFAULT_VALUE
        return ContextCompat.getColor(ctx, colorId)
    }
}