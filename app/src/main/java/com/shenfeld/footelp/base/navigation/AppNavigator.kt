package com.shenfeld.footelp.base.navigation

import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.shenfeld.footelp.base.navigation.comands.SharedTransitionCommand
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import java.util.*

class AppNavigator : SupportAppNavigator {
    private val handler = Handler(Looper.getMainLooper())

    constructor(activity: FragmentActivity, fm: FragmentManager, containerId: Int) : super(
        activity,
        fm,
        containerId
    )

    constructor(activity: FragmentActivity, containerId: Int) : super(activity, containerId)

    override fun applyCommands(commands: Array<Command>) {
        try {
            fragmentManager.executePendingTransactions()
            copyStackToLocal()
            for (command in commands) {
                applyCommand(command)
            }
        } catch (error: IllegalStateException) {
            handler.postDelayed({
                applyCommands(commands)
            }, 200)
        }
    }

    private fun copyStackToLocal() {
        localStackCopy = LinkedList()
        val stackSize = fragmentManager.backStackEntryCount
        for (i in 0 until stackSize) {
            localStackCopy.add(fragmentManager.getBackStackEntryAt(i).name)
        }
    }

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        if (command is SharedTransitionCommand) {
            command.sharedElements.forEach {
                val (view, name) = it
                fragmentTransaction.addSharedElement(view, name)
            }
            nextFragment?.sharedElementEnterTransition = command.transition
        }
    }

    override fun backTo(command: BackTo) {
        if (command.screen == null) {
            backToRoot()
        } else {
            val key = command.screen!!.screenKey
            val index = localStackCopy.indexOf(key)
            val size = localStackCopy.size
            if (index != -1) {
                for (i in 1 until size - index) {
                    localStackCopy.removeLast()
                }
                fragmentManager.popBackStack(key, 0)
            } else {
                if (localStackCopy.size > 0) {
                    fragmentManager.popBackStack()
                    localStackCopy.removeLast()
                }
                fragmentReplace(Replace(command.screen as SupportAppScreen))
            }
        }
    }

    private fun backToRoot() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        localStackCopy.clear()
    }
}