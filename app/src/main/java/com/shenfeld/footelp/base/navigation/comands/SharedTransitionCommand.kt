package com.shenfeld.footelp.base.navigation.comands

import android.view.View
import androidx.transition.Transition

interface SharedTransitionCommand {
    val transition: Transition
    val sharedElements: List<Pair<View, String>>
}