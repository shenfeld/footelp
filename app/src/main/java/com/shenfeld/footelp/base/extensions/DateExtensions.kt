package com.shenfeld.footelp.base.extensions

import org.threeten.bp.*
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

object DateExtensions {
    val ddMMyyyy = "dd/MM/yyyy"
    val ddMMyyyyHHmm = "dd/MMM/yyyy HH:mm"
    val parserFormat = "yyyy-MM-dd"
    val HHmm = "HH:mm"
    val HHmmddMMyyyy = "HH:mm dd.MM.yyyy"
    const val BUNDLE_EXPIRES_DATE_FORMAT = "d 'of' MMMM"
    const val BUNDLE_EXPIRES_PROMO_DATE_FORMAT = "MMMM d"
}

fun LocalDate.formatWithPattern(pattern: String, locale: Locale = Locale.UK): String =
    this.format(DateTimeFormatter.ofPattern(pattern, locale))

fun LocalDateTime.formatWithPattern(pattern: String, locale: Locale = Locale.UK): String =
    this.format(DateTimeFormatter.ofPattern(pattern, locale))

fun LocalDate.autoTopUpFormat(): LocalDate {
    val autoTopUpDate = this.minusDays(1)
    return if (autoTopUpDate.isAfter(LocalDate.now())) {
        autoTopUpDate
    } else {
        autoTopUpDate.plusDays(31)
    }
}

fun Long.toLocalDateTime(): LocalDateTime =
    Instant.ofEpochSecond(this).atZone(ZoneId.systemDefault()).toLocalDateTime()

fun Long.toLocalDate(): LocalDate =
    Instant.ofEpochSecond(this).atZone(ZoneId.systemDefault()).toLocalDate()

fun Long.fromMilliToLocalDateTime(): LocalDateTime =
    Instant.ofEpochMilli(this).atZone(ZoneId.systemDefault()).toLocalDateTime()

fun Long.fromMilliToLocalDate(): LocalDate =
    Instant.ofEpochMilli(this).atZone(ZoneId.systemDefault()).toLocalDate()

fun LocalDate.toOrdinalDate(): String {
    return StringBuilder().also {
        it.append(this.formatWithPattern("MMMM"))
        it.append(" ")
        it.append(this.dayOfMonth.asOrdinalDayOfMonth())
    } .toString()
}

private fun Int.asOrdinalDayOfMonth(): String {
    val suffix = if (this in 11..13) {
        "th"
    } else {
        when (this % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }

    return "${this}$suffix "
}