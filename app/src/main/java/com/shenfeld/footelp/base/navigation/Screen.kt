package com.shenfeld.footelp.base.navigation

import androidx.fragment.app.Fragment
import com.shenfeld.footelp.base.BaseFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

open class Screen(private val fragment: BaseFragment): SupportAppScreen() {
    override fun getFragment(): Fragment? {
        return fragment
    }

    override fun getScreenKey(): String {
        return fragment.getScreenTag()
    }
}