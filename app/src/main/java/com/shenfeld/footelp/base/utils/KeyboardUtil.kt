package com.shenfeld.footelp.base.utils

import android.app.Activity
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewTreeObserver

class KeyboardUtil internal constructor(
    private val decorView: View,
    private val contentView: View,
    private val callback: Callback?,
) {
    private var isEnabled = false
    private var isKeyboardShown = false
    private var isNeedSetPadding = false
    private var lastDiff = 0

    constructor(act: Activity, contentView: View, callback: Callback?) : this(
        act.window.decorView,
        contentView,
        callback
    )

    fun enable() {
        disable()
        isEnabled = true
        decorView.viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
    }

    fun disable() {
        decorView.viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
        isEnabled = false
    }

    fun isEnabled(): Boolean {
        return isEnabled
    }

    fun isKeyboardShown(): Boolean {
        return isKeyboardShown
    }

    fun setNeedSetPadding(needSetPadding: Boolean) {
        isNeedSetPadding = needSetPadding
    }

    private val visibleDisplayFrameRect = Rect()
    private var displayMetrics: DisplayMetrics? = null

    private fun getDisplayMetrics(): DisplayMetrics {
        return displayMetrics ?: run {
            decorView.context.resources.displayMetrics.apply {
                displayMetrics = this
            }
        }
    }

    private val onGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        if (isEnabled.not()) {
            return@OnGlobalLayoutListener
        }
        //visibleDisplayFrameRect will be populated with the coordinates of your view that area still visible.
        decorView.getWindowVisibleDisplayFrame(visibleDisplayFrameRect)

        //get screen height and calculate the difference with the useable area from the visibleDisplayFrameRect
        val height = getDisplayMetrics().heightPixels
        val diff = height - visibleDisplayFrameRect.bottom

        if (diff == lastDiff) {
            return@OnGlobalLayoutListener
        }

        lastDiff = diff

        //if it could be a keyboard add the padding to the view
        if (diff > 0) {

            // if the use-able screen height differs from the total screen height we assume that it shows a keyboard now
            //check if the padding is 0 (if yes set the padding for the keyboard)
            if (contentView.paddingBottom != diff) {
                if (isNeedSetPadding) {
                    //set the padding of the contentView for the keyboard
                    contentView.setPadding(0, 0, 0, diff)
                }
                if (callback != null && isKeyboardShown.not()) {
                    callback.onKeyboardShown(diff)
                }
                isKeyboardShown = true
            }
        } else {
            //check if the padding is != 0 (if yes reset the padding)

            //check if the padding is != 0 (if yes reset the padding)
            if (contentView.paddingBottom != 0 && isNeedSetPadding) {
                //reset the padding of the contentView
                contentView.setPadding(0, 0, 0, 0)
            }
            isKeyboardShown = false
            callback?.onKeyboardHide(diff)
        }
    }

    interface Callback {
        fun onKeyboardShown(diff: Int)
        fun onKeyboardHide(diff: Int)
    }

    class KeyboardVisibilityChangedEvent(val isVisible: Boolean)
}