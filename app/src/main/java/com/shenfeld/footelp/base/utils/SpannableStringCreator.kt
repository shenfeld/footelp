package com.shenfeld.footelp.base.utils

import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils

class SpannableStringCreator {
    private val parts = ArrayList<CharSequence>()
    private var length = 0
    private val spanMap: MutableMap<IntRange, Array<out Any>> = HashMap()

    fun appendSpace(newText: CharSequence) = append(" ").append(newText)

    fun appendSpace(newText: CharSequence, vararg spans: Any) = append(" ").append(newText, *spans)

    fun appendLnNotBlank(newText: CharSequence, vararg spans: Any) = applyIf({ !newText.isBlank() }) { appendLn(newText, *spans) }

    fun appendLn(newText: CharSequence, vararg spans: Any) = append("\n").append(newText, *spans)

    fun append(newText: CharSequence, vararg spans: Any) = apply {
        val end = newText.length
        parts.add(newText)
        spanMap[(length..length + end)] = spans
        length += end
    }

    fun append(newText: CharSequence) = apply {
        parts.add(newText)
        length += newText.length
    }

    inline fun applyIf(predicate: () -> Boolean, action: SpannableStringCreator.() -> SpannableStringCreator) = if (predicate()) action() else this

    fun toSpannableString() = SpannableString(TextUtils.concat(*parts.toTypedArray())).apply {
        spanMap.forEach {
            val range = it.key
            it.value.forEach {item ->
                setSpan(item, range.first, range.last, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }
}

fun applySpan(text: CharSequence, vararg spans: Any): SpannableString {
    return SpannableStringCreator()
        .append(text, *spans)
        .toSpannableString()
}