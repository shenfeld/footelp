package com.shenfeld.footelp.base.navigation

interface Loader {
    fun showLoader()
    fun hideLoader()
}