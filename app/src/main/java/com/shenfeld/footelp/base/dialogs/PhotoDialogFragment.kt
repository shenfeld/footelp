package com.shenfeld.footelp.base.dialogs

import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.ColorProvider
import com.shenfeld.footelp.base.extensions.onClick
import kotlinx.android.synthetic.main.fragment_dialog_take_photo.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PhotoDialogFragment : DialogFragment(), KoinComponent {

    companion object {
        private const val TAG = "EnableBiometricDialog"
    }

    private val colorProvider: ColorProvider by inject()

    private lateinit var bitmap: Bitmap
    private lateinit var onTakePhoto: () -> Unit
    private lateinit var onPickPhoto: () -> Unit

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = true
        return inflater.inflate(R.layout.fragment_dialog_take_photo, container, true)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupListeners()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            it.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            it.setGravity(Gravity.BOTTOM)
            it.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupView() {
        ivBlur.foreground = ColorDrawable(colorProvider.getColor(R.color.black_99000000))
        ivBlur.setImageBitmap(bitmap)
    }

    private fun setupListeners() {
        ivBlur.onClick {
            dismiss()
        }

        btnTakePhoto.onClick {
            onTakePhoto.invoke()
            dismiss()
        }

        btnPickPhoto.onClick {
            onPickPhoto.invoke()
            dismiss()
        }
    }

    fun showWithBackground(
        background: Bitmap,
        fragmentManager: FragmentManager,
        onTakePhoto: () -> Unit,
        onPickPhoto: () -> Unit
    ) {
        with(PhotoDialogFragment()) {
            this.bitmap = background
            this.onTakePhoto = onTakePhoto
            this.onPickPhoto = onPickPhoto
            show(fragmentManager, TAG)
        }
    }

}