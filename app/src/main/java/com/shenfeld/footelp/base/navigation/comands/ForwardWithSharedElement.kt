package com.shenfeld.footelp.base.navigation.comands

import android.view.View
import androidx.transition.Transition
import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.commands.Forward

class ForwardWithSharedElement(
    screen: Screen,
    override val transition: Transition,
    override val sharedElements: List<Pair<View, String>>
) : Forward(screen), SharedTransitionCommand