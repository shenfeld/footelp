@file:Suppress("unused")

package com.shenfeld.footelp.base.extensions

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.WindowManager

fun Context.getScreenWidth(): Int {
    return getDisplayMetrics().widthPixels
}

fun Context.getScreenHeight(): Int {
    return getDisplayMetrics().heightPixels
}

fun Context.getDisplayMetrics(): DisplayMetrics {
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = windowManager.defaultDisplay
    val metrics = DisplayMetrics()
    display.getMetrics(metrics)
    return metrics
}

fun Context.convertDpToPixel(dp: Float): Float {
    return dp * (resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun Context.convertPixelsToDp(px: Float): Float {
    return px / (resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

val Float.dp: Int
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        Resources.getSystem().displayMetrics
    ).toInt()

fun Context.getSdpAsPixel(sdp: Int): Int {
    val id = resources.getIdentifier("_${sdp}sdp", "dimen", packageName)
    return resources.getDimensionPixelSize(id)
}

fun Context.getSdpAsPixelFloat(sdp: Int): Float {
    val id = resources.getIdentifier("_${sdp}sdp", "dimen", packageName)
    return resources.getDimension(id)
}

fun Context.getSdpAsDp(sdp: Int): Float {
    return convertPixelsToDp(getSdpAsPixelFloat(sdp))
}