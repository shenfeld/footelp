package com.shenfeld.footelp.base.dialogs

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.ColorProvider
import com.shenfeld.footelp.base.extensions.onClick
import kotlinx.android.synthetic.main.fragment_dialog_result.*
import kotlinx.android.synthetic.main.fragment_dialog_result.ivBack
import org.koin.android.ext.android.inject
import timber.log.Timber
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.properties.Delegates

class ResultDialogFragment : DialogFragment() {

    companion object {
        private const val TAG = "ResultDialogFragment"
    }

    private val colorProvider: ColorProvider by inject()

    private var probability by Delegates.notNull<Float>()
    private lateinit var bitmap: Bitmap

    @StringRes
    private var textInfo: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = true
        return inflater.inflate(R.layout.fragment_dialog_result, container, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupListeners()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            it.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            it.setGravity(Gravity.CENTER)
            it.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    @SuppressLint("SetTextI18n", "TimberArgCount")
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun setupView() {
        ivBlur.foreground = ColorDrawable(colorProvider.getColor(R.color.black_99000000))
        ivBlur.setImageBitmap(bitmap)
        tvTextInfo.setText(textInfo)
        tvTextInfo.justificationMode = JUSTIFICATION_MODE_INTER_WORD
        tvProbability.text = "${(this.probability * 1000).roundToInt() / 1000.0} %"
        if (probability < 75 && probability >= 45) {
            tvProbability.setTextColor(colorProvider.getColor(R.color.brown_F0B785))
        }
        if (probability < 45) {
            tvProbability.setTextColor(colorProvider.getColor(R.color.red_B22222))
        }
    }

    private fun setupListeners() {
        ivBlur.onClick {
            dismiss()
        }

        ivBack.onClick {
            dismiss()
        }
    }

    fun showWithBackground(
        background: Bitmap,
        fragmentManager: FragmentManager,
        textInfoRes: Int,
        probability: Float,
    ) {
        with(ResultDialogFragment()) {
            this.bitmap = background
            this.textInfo = textInfoRes
            this.probability = probability * 100
            show(fragmentManager, TAG)
        }
    }

}