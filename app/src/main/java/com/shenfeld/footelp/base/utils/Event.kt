package com.shenfeld.footelp.base.utils

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class Event<out T>(private val content: T) {

    var isHandled = false
        private set // Allow external read but not write

    fun getValue(): T? {
        return if (isHandled) {
            null
        } else {
            isHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}