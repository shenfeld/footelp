package com.shenfeld.footelp.base

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.CallSuper
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import com.shenfeld.footelp.base.extensions.hideKeyboard
import com.shenfeld.footelp.base.navigation.AppRouter
import com.shenfeld.footelp.base.navigation.BackButtonListener
import com.shenfeld.footelp.base.navigation.Loader
import org.koin.android.ext.android.inject

abstract class BaseFragment : Fragment(), BackButtonListener {
    abstract val layoutId: Int?
    open val isBottomNavVisible = false

    val appRouter: AppRouter by inject()
    protected open val resourcesManager: ResourcesManager by inject()
    protected val colorProvider: ColorProvider by inject()

    private var isBackNavEnabled = true
    private val onResumeActions = arrayListOf<Runnable>()
    private val onAttachActions = arrayListOf<Runnable>()
    private var isCommitsAllowed: Boolean = false
    private var isAttached: Boolean = false

    companion object {
        private const val LOADER_DELAY_MILLIS = 2000L
    }

    override fun onAttach(context: Context) {
        isAttached = true
        onAttachActions.forEach { it.run() }
        onAttachActions.clear()
        super.onAttach(context)
    }

    override fun onDetach() {
        isAttached = false
        onAttachActions.clear()
        super.onDetach()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layoutId?.let {
            return inflater.inflate(it, container, false)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val activity = requireActivity() as? BottomNavState?
//        if (isBottomNavVisible) activity?.showBottomNav() else activity?.hideBottomNav()
    }

    @CallSuper
    override fun onResume() {
        super.onResume()
        isCommitsAllowed = true
        onResumeActions.forEach { it.run() }
        onResumeActions.clear()
    }

    override fun onDestroyView() {
        activity?.hideKeyboard()
        hideLoader()
        super.onDestroyView()
    }

    override fun onBackPressed(): Boolean {
        if (isBackNavEnabled) {
            appRouter.exit()
        }
        return true
    }

    protected fun disableBackNav() {
        isBackNavEnabled = false
    }

    protected fun <T> LiveData<T>.subscribe(observer: (result: T) -> Unit) {
        this.observe(this@BaseFragment.viewLifecycleOwner, observer)
    }

    fun runOnViewAvailable(action: Runnable) {
        if (isCommitsAllowed) {
            action.run()
        } else {
            onResumeActions.add(action)
        }
    }

    fun runOnAttach(action: Runnable) {
        if (isAttached) {
            action.run()
        } else {
            onAttachActions.add(action)
        }
    }

    protected fun makeScreenTouchable() {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    protected fun makeScreenUnTouchable() {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    private val handler by lazy { Handler(Looper.getMainLooper()) }

    protected fun showLoader() {
        handler.postDelayed({
            (activity as? Loader)?.showLoader()
        }, LOADER_DELAY_MILLIS)
    }

    protected fun hideLoader() {
        handler.removeCallbacksAndMessages(null)
        (activity as? Loader)?.hideLoader()
    }

    fun getScreenTag(): String = javaClass.canonicalName ?: ""

    fun View.safePost(block: () -> Unit) {
        this.post {
            if (isVisible) {
                block.invoke()
            }
        }
    }

    fun View.safePostDelayed(delay: Long, block: () -> Unit) {
        this.postDelayed({
            if (isVisible) {
                block.invoke()
            }
        }, delay)
    }

    protected fun getColor(@ColorRes id: Int) =
        ContextCompat.getColor(requireContext(), id)
}