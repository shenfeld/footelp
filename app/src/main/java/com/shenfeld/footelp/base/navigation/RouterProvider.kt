package com.shenfeld.footelp.base.navigation

interface RouterProvider {

    val router: AppRouter

}