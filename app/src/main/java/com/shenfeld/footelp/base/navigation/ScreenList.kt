package com.shenfeld.footelp.base.navigation

import ru.terrakok.cicerone.Screen

interface ScreenList {
    val footelpScreens: FootelpScreens
    interface FootelpScreens {
        fun home(): Screen
        fun infoApp(): Screen
        fun allowanceCamera(): Screen
        fun history(): Screen
    }
}