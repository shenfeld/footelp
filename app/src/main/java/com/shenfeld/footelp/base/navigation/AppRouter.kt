package com.shenfeld.footelp.base.navigation

import android.view.View
import androidx.transition.Transition
import com.shenfeld.footelp.base.navigation.comands.ForwardWithSharedElement
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen

class AppRouter : Router() {
    companion object {
        private val resultListeners = mutableMapOf<Int, (Any?) -> Unit>()
    }

    fun navigateTo(
        screen: Screen,
        transition: Transition,
        vararg sharedElements: Pair<View, String>
    ) {
        val listOfSharedElement = mutableListOf<Pair<View, String>>()
        sharedElements.forEach {
            listOfSharedElement.add(it)
        }
        executeCommands(ForwardWithSharedElement(screen, transition, listOfSharedElement))
    }

    fun openForResult(
        screen: Screen,
        resultCode: Int,
        onResultCallback: (any: Any?) -> Unit
    ) {
        setResultListener(resultCode) {
            onResultCallback.invoke(it)
            removeResultListener(resultCode)
        }
        navigateTo(screen)
    }

    fun exitWithResult(resultCode: Int, result: Any?) {
        exit()
        sendResult(resultCode, result)
    }

    private fun setResultListener(resultCode: Int, listener: (Any?) -> Unit) {
        resultListeners[resultCode] = listener
    }

    private fun removeResultListener(resultCode: Int) {
        resultListeners.remove(resultCode)
    }

    private fun sendResult(resultCode: Int, result: Any?) {
        resultListeners[resultCode]?.invoke(result)
    }
}