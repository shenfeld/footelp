package com.shenfeld.footelp.base.navigation

interface BackButtonListener {

    fun onBackPressed(): Boolean

}