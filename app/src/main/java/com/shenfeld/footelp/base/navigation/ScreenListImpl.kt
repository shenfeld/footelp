package com.shenfeld.footelp.base.navigation

import com.shenfeld.footelp.app.fragments.allowance_camera.AllowanceCameraFragment
import com.shenfeld.footelp.app.fragments.footelp_info.FootelpInfoFragment
import com.shenfeld.footelp.app.fragments.history_screen.HistoryFragment
import com.shenfeld.footelp.app.fragments.home.HomeFragment
import org.koin.core.component.KoinApiExtension
import ru.terrakok.cicerone.Screen

@KoinApiExtension
class ScreenListImpl : ScreenList {
    override val footelpScreens = object : ScreenList.FootelpScreens {
        override fun home(): Screen = Screen(HomeFragment.newInstance())
        override fun infoApp(): Screen = Screen(FootelpInfoFragment.newInstance())
        override fun allowanceCamera(): Screen = Screen(AllowanceCameraFragment.newInstance())
        override fun history(): Screen = Screen(HistoryFragment.newInstance())
    }
}
