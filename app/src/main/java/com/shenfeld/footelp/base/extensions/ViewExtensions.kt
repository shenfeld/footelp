package com.shenfeld.footelp.base.extensions

import android.animation.*
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.view.View
import android.view.ViewGroup
import android.widget.OverScroller
import android.widget.ScrollView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.core.view.*
import androidx.customview.widget.ViewDragHelper
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import timber.log.Timber


const val DEFAULT_DELAY_MS = 750L

inline fun View.onClickDebounce(delayMs: Long = DEFAULT_DELAY_MS, crossinline l: (View?) -> Unit) {
    setOnClickListener(object : View.OnClickListener {
        private var notClicked = true
        override fun onClick(view: View) {
            if (notClicked) {
                notClicked = false
                l(view)
                view.postDelayed({ notClicked = true }, delayMs)
            }
        }
    })
}

inline fun View.onClick(crossinline l: (View?) -> Unit) = onClickDebounce(DEFAULT_DELAY_MS, l)

fun View.show() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.blur(radius: Float, times: Int = 1, result: ((Bitmap) -> Unit)) {
    post {
        try {
            val bitmap = this.drawToBitmap(Bitmap.Config.ARGB_8888)
            repeat(times = times) {
                val renderScript = RenderScript.create(context)
                val input = Allocation.createFromBitmap(renderScript, bitmap)
                val output = Allocation.createTyped(renderScript, input.type)
                val script = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))
                script.setRadius(radius)
                script.setInput(input)
                script.forEach(output)
                output.copyTo(bitmap)
            }
            result.invoke(bitmap)
        } catch (e: IllegalStateException) {
            Timber.e(e)
        }
    }
}

fun View.tint(color: Int) {
    val colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
        color,
        BlendModeCompat.SRC_ATOP
    )
    background.colorFilter = colorFilter
}

fun tintViews(color: Int, vararg view: View) {
    view.forEach { it.tint(color) }
}

fun View.updateMargin(
    left: Int = marginLeft,
    top: Int = marginTop,
    right: Int = marginRight,
    bottom: Int = marginBottom
) = updateLayoutParams<ViewGroup.MarginLayoutParams> { updateMargins(left, top, right, bottom) }


fun View.applyAnimationForChildren(animation: View.() -> Animator): Animator {
    if (this is ViewGroup) {
        val listOfAnimators = arrayListOf<Animator>()
        this.forEach { child ->
            val animator: Animator
            if (child is ViewGroup) {
                listOfAnimators.add(child.animation())
                animator = child.applyAnimationForChildren(animation)
            } else {
                animator = child.animation()
            }
            listOfAnimators.add(animator)
        }

        return AnimatorSet().apply {
            if (listOfAnimators.isNotEmpty()) {
                playTogether(*listOfAnimators.toTypedArray())
                duration = listOfAnimators[0].duration
            }
        }
    } else {
        throw  RuntimeException("This view is not instance of ViewGroup")
    }
}

fun CharSequence.getDigitsCountAfterPoint(): Int {
    return if (this.contains(".")) {
        val indexOfPoint = this.indexOf(".")
        this.substring(indexOfPoint, this.length).length
    } else {
        0
    }
}

fun String.phoneFormat() =
    replaceFirst("44", "0").replaceFirst(".....".toRegex(), "$0 ")


fun <T : View> BottomSheetBehavior<T>.getViewDragHelper(): ViewDragHelper? =
    BottomSheetBehavior::class.java
        .getDeclaredField("viewDragHelper")
        .apply { isAccessible = true }
        .let { field -> field.get(this) as? ViewDragHelper? }

fun ViewDragHelper.getScroller(): OverScroller? =
    ViewDragHelper::class.java
        .getDeclaredField("mScroller")
        .apply { isAccessible = true }
        .let { field -> field.get(this) as? OverScroller? }

fun ScrollView.scrollToBottom() {
    smoothScrollBy(0, this.distanceToBottom())
}

fun ScrollView.distanceToBottom(): Int {
    val lastChild = getChildAt(childCount - 1)
    val bottom = lastChild.bottom + paddingBottom
    return bottom - (scrollY + height)
}

fun TextView.setDrawableColorRes(@ColorRes colorRes: Int) {
    setDrawableColor(ContextCompat.getColor(context, colorRes))
}

fun TextView.setDrawableColor(@ColorInt color: Int) {
    compoundDrawablesRelative.forEach { drawable ->
        if (drawable != null) {
            drawable.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
        }
    }
}

fun FloatingActionButton.animateWithTranslation(trX: Float, trY: Float) {
    val oa = ObjectAnimator.ofPropertyValuesHolder(
        this,
        PropertyValuesHolder.ofFloat("translationX", translationX, trX),
        PropertyValuesHolder.ofFloat("translationY", translationY, trY)
    ).apply {
        duration = 300L
        interpolator = FastOutSlowInInterpolator()
    }

    oa.start()
}

fun FloatingActionButton.init(trX: Float, trY: Float) {
    visibility = View.INVISIBLE
    addOnShowAnimationListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator?) {
            animateWithTranslation(trX, trY)
            super.onAnimationStart(animation)
        }
    })

    addOnHideAnimationListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator?) {
            animateWithTranslation(0f, 0f)
            super.onAnimationStart(animation)
        }
    })
}
