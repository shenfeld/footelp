package com.shenfeld.footelp.base

import androidx.annotation.ColorRes

interface ColorProvider {
    fun getColor(@ColorRes colorId: Int) : Int
}