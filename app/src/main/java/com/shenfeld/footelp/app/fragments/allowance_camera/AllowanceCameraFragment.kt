package com.shenfeld.footelp.app.fragments.allowance_camera

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.style.BackgroundColorSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.BaseFragment
import com.shenfeld.footelp.base.extensions.onClick
import com.shenfeld.footelp.base.utils.SpanFormatter
import com.shenfeld.footelp.base.utils.applySpan
import kotlinx.android.synthetic.main.fragment_allowance_camera.*
import kotlinx.android.synthetic.main.fragment_allowance_camera.ivBack
import kotlinx.android.synthetic.main.fragment_allowance_camera.ivFootelp
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension
import render.animations.Fade
import render.animations.Render

@KoinApiExtension
class AllowanceCameraFragment : BaseFragment() {
    override val layoutId: Int = R.layout.fragment_allowance_camera
    private val viewModel: AllowanceCameraViewModel by viewModel()
    private val animator by lazy { Render(requireContext()) }

    companion object {
        fun newInstance() = AllowanceCameraFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.window?.statusBarColor = getColor(R.color.white)
        activity?.window?.navigationBarColor = getColor(R.color.white)
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupListeners()
    }

    override fun onBackPressed(): Boolean {
        return ivBack.performClick()
    }

    override fun onResume() {
        super.onResume()
        if (isPermissionGranted()) {
            viewModel.onBackClicked()
        }
    }

    override fun onDestroyView() {
        activity?.window?.statusBarColor = getColor(R.color.blue_5588E7)
        activity?.window?.navigationBarColor = getColor(R.color.blue_3C6FCE)
        super.onDestroyView()
    }

    private fun setupView() {
        animator.setAnimation(Fade().InLeft(ivBack))
        animator.start()
        animator.setAnimation(Fade().InRight(ivFootelp))
        animator.start()
        animator.setAnimation(Fade().InDown(tvTitle))
        animator.start()
        animator.setAnimation(Fade().InDown(btnToSettings))
        animator.start()

        tvTitle.text = SpanFormatter.format(
            format = getString(R.string.allowance_camera_title),
            applySpan(
                text = getString(R.string.allowance_camera_access),
                BackgroundColorSpan(getColor(R.color.colorAccent))
            )
        )
    }

    private fun setupListeners() {
        ivBack.onClick {
            viewModel.onBackClicked()
        }

        btnToSettings.onClick {
            toSettingsClick()
        }
    }

    private fun toSettingsClick() {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + activity?.packageName)
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun isPermissionGranted(): Boolean {
        val cameraPermission =
            ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
        val storagePermission = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
        return cameraPermission == PackageManager.PERMISSION_GRANTED && storagePermission == PackageManager.PERMISSION_GRANTED
    }
}