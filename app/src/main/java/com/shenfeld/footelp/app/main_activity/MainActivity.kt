package com.shenfeld.footelp.app.main_activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.navigation.*
import com.shenfeld.footelp.base.utils.KeyboardUtil
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.Cicerone

class MainActivity : AppCompatActivity(),
    RouterProvider,
    Loader,
    ActivityCompat.OnRequestPermissionsResultCallback,
    KeyboardUtil.Callback {

    private val navigator = AppNavigator(this, R.id.mainContainer)
    private val cicerone: Cicerone<AppRouter> by inject()
    override val router: AppRouter by inject()
    private val viewModel by viewModel<MainViewModel>()
    private lateinit var keyboardUtil: KeyboardUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        keyboardUtil = KeyboardUtil(this, rootView, this)
        viewModel.onIntentData(null,false)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent == null || intent.data == null || !intent.data.toString().contains("ttmm"))
            return
        val data = intent.data
        viewModel.onIntentData(data!!, false)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        cicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onResume() {
        super.onResume()
        keyboardUtil.enable()
    }

    override fun onPause() {
        keyboardUtil.disable()
        cicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
        if ((fragment != null && fragment is BackButtonListener && fragment.onBackPressed())) {
            return
        } else {
            router.exit()
        }
        super.onBackPressed()
    }

    override fun showLoader() {

    }

    override fun hideLoader() {

    }

    override fun onKeyboardShown(diff: Int) {

    }

    override fun onKeyboardHide(diff: Int) {

    }
}