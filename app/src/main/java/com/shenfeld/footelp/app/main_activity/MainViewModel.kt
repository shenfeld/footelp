package com.shenfeld.footelp.app.main_activity

import android.net.Uri
import com.shenfeld.footelp.base.BaseViewModel
import timber.log.Timber

class MainViewModel : BaseViewModel() {

    fun onIntentData(uri: Uri?, firstStart: Boolean) {
        uri?.let {
//            val deepLink = deepLinkProcessor.parse(uri)
            Timber.tag("DL Processor Result::").d(uri.toString())
        }
            ?: run {
                showFirstScreen()
            }
    }

    private fun showFirstScreen() {
        appRouter.newRootScreen(screenList.footelpScreens.home())
    }
}