package com.shenfeld.footelp.app.fragments.home

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import com.shenfeld.footelp.base.BaseViewModel
import com.shenfeld.footelp.base.extensions.asEvent
import com.shenfeld.footelp.base.extensions.getDigitsCountAfterPoint
import com.shenfeld.footelp.base.utils.Event
import com.shenfeld.footelp.imageprocessor.Recognizer
import org.koin.core.component.KoinApiExtension
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import timber.log.Timber


@KoinApiExtension
class HomeViewModel : BaseViewModel() {
    val showBottomDialogLiveData = MutableLiveData<Event<Unit>>()
    val setReadyStateLiveData = MutableLiveData<Event<Recognizer.Recognition>>()

    fun infoAppClicked() {
        appRouter.navigateTo(screenList.footelpScreens.infoApp())
    }

    fun analyzeFootClicked(permissionGranted: Boolean) {
        if (permissionGranted) {
            showBottomDialogLiveData.value = Unit.asEvent()
        } else {
            appRouter.navigateTo(screenList.footelpScreens.allowanceCamera())
        }
    }

    fun historyClicked() {
        appRouter.navigateTo(screenList.footelpScreens.history())
    }

    fun processRecognitions(recognizeImage: List<Recognizer.Recognition>) {
        setReadyStateLiveData.value = recognizeImage[0].asEvent()
    }
}