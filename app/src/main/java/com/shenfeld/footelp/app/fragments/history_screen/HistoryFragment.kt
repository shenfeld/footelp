package com.shenfeld.footelp.app.fragments.history_screen

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.BaseFragment
import com.shenfeld.footelp.base.extensions.onClick
import com.shenfeld.footelp.app.fragments.history_screen.history_adapter.HistoryAdapter
import kotlinx.android.synthetic.main.fragment_history.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension
import render.animations.Fade
import render.animations.Render

@KoinApiExtension
class HistoryFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_history
    private val viewModel: HistoryViewModel by viewModel()
    private val animator by lazy { Render(requireContext()) }
    private lateinit var historyAdapter: HistoryAdapter

    companion object {
        fun newInstance() = HistoryFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.window?.statusBarColor = getColor(R.color.white)
        activity?.window?.navigationBarColor = getColor(R.color.white)
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupObservers()
        setupListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.window?.statusBarColor = getColor(R.color.blue_5588E7)
        activity?.window?.navigationBarColor = getColor(R.color.blue_3C6FCE)
    }

    override fun onBackPressed(): Boolean {
        return ivBack.performClick()
    }

    private fun setupView() {
        animator.setAnimation(Fade().InLeft(ivBack))
        animator.start()
        animator.setAnimation(Fade().InRight(ivFootelp))
        animator.start()
        animator.setAnimation(Fade().InDown(rvHistory))
        animator.start()

        val linearLayoutManager = LinearLayoutManager(context)
        rvHistory.layoutManager = linearLayoutManager
        historyAdapter = HistoryAdapter(viewModel::onHistoryItemClicked)
        rvHistory.adapter = historyAdapter
    }

    private fun setupObservers() {
        viewModel.setHistoryLiveData.observe(
            viewLifecycleOwner,
            historyAdapter::setHistory
        )

        viewModel.showToastLiveData.observe(viewLifecycleOwner) { text ->
            showToast(text)
        }
    }

    private fun setupListeners() {
        ivBack.onClick {
            viewModel.onBackClicked()
        }
    }

    private fun showToast(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }
}