package com.shenfeld.footelp.app.fragments.home

import android.view.View
import androidx.interpolator.view.animation.FastOutSlowInInterpolator

class HomeAnimator {
    fun animateFadeIn(view: View, delay: Long = 0) {
        view.animate().apply {
            interpolator = FastOutSlowInInterpolator()
            duration = 250L
            alpha(1f)
            startDelay = delay
            start()
        }
    }
}