package com.shenfeld.footelp.app.fragments.footelp_info

import com.shenfeld.footelp.base.BaseViewModel
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class FootelpInfoViewModel : BaseViewModel() {
    fun onBackClicked() {
        appRouter.exit()
    }
}