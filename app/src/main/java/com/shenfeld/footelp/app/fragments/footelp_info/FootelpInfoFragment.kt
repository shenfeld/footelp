package com.shenfeld.footelp.app.fragments.footelp_info

import android.Manifest
import android.animation.AnimatorSet
import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Bundle
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.BaseFragment
import com.shenfeld.footelp.base.extensions.onClick
import com.shenfeld.footelp.base.utils.SpanFormatter
import com.shenfeld.footelp.base.utils.applySpan
import kotlinx.android.synthetic.main.fragment_footelp_info.*
import kotlinx.android.synthetic.main.fragment_footelp_info.ivFootelp
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension
import render.animations.Fade
import render.animations.Render

@KoinApiExtension
class FootelpInfoFragment : BaseFragment() {
    override val layoutId: Int = R.layout.fragment_footelp_info
    private val viewModel: FootelpInfoViewModel by viewModel()
    private val animator by lazy { Render(requireContext()) }

    companion object {
        fun newInstance() = FootelpInfoFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupListeners()
    }

    override fun onBackPressed(): Boolean {
        return ivBack.performClick()
    }

    private fun setupView() {
        tvTitle.text = SpanFormatter.format(
            format = getString(R.string.footelp_info_title),
            applySpan(
                text = getString(R.string.footelp_info_help_you),
                BackgroundColorSpan(colorProvider.getColor(R.color.white)),
                ForegroundColorSpan(colorProvider.getColor(R.color.pink_F272A5))
            )
        )

        animator.setAnimation(Fade().InLeft(ivBack))
        animator.start()
        animator.setAnimation(Fade().InRight(ivFootelp))
        animator.start()
        animator.setAnimation(Fade().InDown(tvTitle))
        animator.start()
        animator.setAnimation(Fade().InDown(tvVersionInfo))
        animator.start()
    }

    private fun setupListeners() {
        ivBack.onClick {
            viewModel.onBackClicked()
        }
    }
}