package com.shenfeld.footelp.app.fragments.history_screen.history_adapter

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class HistoryAdapter(
    vararg adapters: AdapterDelegate<List<Any>>
) : ListDelegationAdapter<List<Any>>(*adapters) {

    init {
        items = emptyList()
    }

    constructor(
        onHistoryItemClicked: (HistoryItem) -> Unit
    ) : this(
        historyAdapterDelegate(onHistoryItemClicked)
    )

    fun setHistory(historyItems: List<Any>) {
        items = items
            .toMutableList()
            .apply {
                removeAll { it is HistoryItem }
                addAll(historyItems)
            }
        notifyDataSetChanged()
    }

}