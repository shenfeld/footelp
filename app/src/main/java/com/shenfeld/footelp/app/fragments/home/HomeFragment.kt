package com.shenfeld.footelp.app.fragments.home

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.BaseFragment
import com.shenfeld.footelp.base.dialogs.PhotoDialogFragment
import com.shenfeld.footelp.base.dialogs.ResultDialogFragment
import com.shenfeld.footelp.base.extensions.blur
import com.shenfeld.footelp.base.extensions.init
import com.shenfeld.footelp.base.extensions.observeEvent
import com.shenfeld.footelp.imageprocessor.Recognizer
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension
import render.animations.Fade
import render.animations.Render
import timber.log.Timber


@KoinApiExtension
class HomeFragment : BaseFragment() {
    override val layoutId: Int = R.layout.fragment_home
    private val viewModel: HomeViewModel by viewModel()
    private val animator = HomeAnimator()
    private val recognizer by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        Recognizer(context = requireContext())
    }

    private val pickPhoto =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let { uriImage ->
                val bitmap =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        ImageDecoder.decodeBitmap(
                            ImageDecoder.createSource(requireContext().contentResolver, uriImage)
                        )
                    } else {
                        MediaStore.Images.Media.getBitmap(
                            requireContext().contentResolver,
                            uriImage
                        )
                    }
                lifecycleScope.launchWhenResumed {
                    setProcessingState(isProcessing = true)
                    val recognitions =
                        recognizer.recognizeImage(img = bitmap.copy(config = Bitmap.Config.ARGB_8888, isMutable =true))
                    viewModel.processRecognitions(recognitions)
                }
            }
        }

    private val cameraShot =
        registerForActivityResult(ActivityResultContracts.TakePicturePreview()) { bitmap ->
            if (bitmap != null) {
                lifecycleScope.launchWhenResumed {
                    setProcessingState(isProcessing = true)
                    val recognitions =
                        recognizer.recognizeImage(img = bitmap.copy(Bitmap.Config.ARGB_8888, true))
                    viewModel.processRecognitions(recognitions)
                }
            }
        }

    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> {
                    // user granted permission
                    cameraShot.launch()
                }
                !shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> {
                    // user denied permission and set Don't ask again.
                    viewModel.analyzeFootClicked(isPermissionGranted())
                }
                else -> {
                    showToast(getString(R.string.denied_toast))
                }
            }
        }

    private var isImageSet: Boolean = false

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.window?.statusBarColor = getColor(R.color.blue_5588E7)
        activity?.window?.navigationBarColor = getColor(R.color.blue_3C6FCE)
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupListeners()
        setupObservers()
    }

    private fun setupView() {
        val render = Render(requireContext())
        animator.animateFadeIn(ivLeftTopWave)
        render.setAnimation(Fade().InDown(ivFootelp))
        render.start()
        animator.animateFadeIn(ivRightBottomWave, delay = 600L)
    }

    private fun setupListeners() {
        fabHistory.init(-236f, -236f)
        fabAnalyzeFoot.init(0f, -318f)
        fabInfoApp.init(236f, -236f)


        fabMain.setOnClickListener {
            if (fabHistory.isOrWillBeShown) fabHistory.hide() else fabHistory.show()
            if (fabAnalyzeFoot.isOrWillBeShown) fabAnalyzeFoot.hide() else fabAnalyzeFoot.show()
            if (fabInfoApp.isOrWillBeShown) fabInfoApp.hide() else fabInfoApp.show()
        }

        fabInfoApp.setOnClickListener {
            viewModel.infoAppClicked()
        }

        fabAnalyzeFoot.setOnClickListener {
            viewModel.analyzeFootClicked(isPermissionGranted())
        }

        fabHistory.setOnClickListener {
            viewModel.historyClicked()
        }
    }

    private fun setupObservers() {
        viewModel.showBottomDialogLiveData.observeEvent(viewLifecycleOwner) {
            showPhotoDialog()
        }

        viewModel.setReadyStateLiveData.observeEvent(viewLifecycleOwner) {
            setProcessingState(isProcessing = false)
            requireView().blur(20F) { bitmap ->
                ResultDialogFragment().showWithBackground(
                    background = bitmap,
                    fragmentManager = childFragmentManager,
                    textInfoRes = it.title,
                    probability = it.confidence,
                )
            }
        }
    }

    private fun isPermissionGranted(): Boolean {
        val cameraPermission =
            ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
        val storagePermission = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        return cameraPermission == PackageManager.PERMISSION_GRANTED && storagePermission == PackageManager.PERMISSION_GRANTED
    }

    private fun showPhotoDialog() {
        requireView().blur(20F) { bitmap ->
            PhotoDialogFragment().showWithBackground(
                background = bitmap,
                fragmentManager = childFragmentManager,
                onTakePhoto = { takePhoto() },
                onPickPhoto = { pickPhoto() }
            )
        }
    }

    private fun pickPhoto() {
        pickPhoto.launch("image/*")
    }

    private fun takePhoto() {
        takePhoto.launch(Manifest.permission.CAMERA)
    }

    private fun showToast(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    private fun setViewVisibility(isVisible: Boolean): Int {
        return if (isVisible) View.VISIBLE else View.GONE
    }

    private fun setProcessingState(isProcessing: Boolean) {
        if (!isImageSet) {
            isImageSet = true
            requireView().blur(20F, 3) {
                vOverlay.foreground = ColorDrawable(colorProvider.getColor(R.color.black_99000000))
                vOverlay.setImageBitmap(it)
            }
        }
        vOverlay.visibility = setViewVisibility(isProcessing)
        lavAnalyze.visibility = setViewVisibility(isProcessing)
    }
}