package com.shenfeld.footelp.app.fragments.history_screen

import androidx.lifecycle.MutableLiveData
import com.shenfeld.footelp.base.BaseViewModel
import com.shenfeld.footelp.app.fragments.history_screen.history_adapter.HistoryItem
import org.koin.core.component.KoinApiExtension
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime

@KoinApiExtension
class HistoryViewModel : BaseViewModel() {
    val showToastLiveData = MutableLiveData<String>()
    val setHistoryLiveData = MutableLiveData<List<Any>>()
    private val history = listOf<Any>(
        HistoryItem(
                analyzeId = 1,
            date = LocalDateTime.now(),
            status = HistoryItem.Status.HEALTHY
        ),
        HistoryItem(
            analyzeId = 2,
            date = LocalDateTime.now().minusDays(1),
            status = HistoryItem.Status.MINOR_DEFORMATION
        ),
        HistoryItem(
            analyzeId = 3,
            date = LocalDateTime.now().minusDays(2),
            status = HistoryItem.Status.DEFORMED
        ),
        HistoryItem(
            analyzeId = 4,
            date = LocalDateTime.now().minusDays(3),
            status = HistoryItem.Status.SIGNIFICANT_DEFORMATION
        ),
        HistoryItem(
            analyzeId = 5,
            date = LocalDateTime.now().minusDays(4),
            status = HistoryItem.Status.SEE_A_DOCTOR
        ),
        HistoryItem(
            analyzeId = 6,
            date = LocalDateTime.now().minusDays(5),
            status = HistoryItem.Status.SIGNIFICANT_DEFORMATION
        ),
        HistoryItem(
            analyzeId = 7,
            date = LocalDateTime.now().minusDays(6),
            status = HistoryItem.Status.HEALTHY
        ),
        HistoryItem(
            analyzeId = 8,
            date = LocalDateTime.now().minusDays(7),
            status = HistoryItem.Status.DEFORMED
        ),
        HistoryItem(
            analyzeId = 9,
            date = LocalDateTime.now().minusDays(8),
            status = HistoryItem.Status.HEALTHY
        ),
        HistoryItem(
            analyzeId = 10,
            date = LocalDateTime.now().minusDays(9),
            status = HistoryItem.Status.DEFORMED
        ),
        HistoryItem(
            analyzeId = 11,
            date = LocalDateTime.now().minusDays(10),
            status = HistoryItem.Status.HEALTHY
        ),
        HistoryItem(
            analyzeId = 12,
            date = LocalDateTime.now().minusDays(11),
            status = HistoryItem.Status.MINOR_DEFORMATION
        ),
        HistoryItem(
            analyzeId = 13,
            date = LocalDateTime.now().minusDays(12),
            status = HistoryItem.Status.HEALTHY
        ),
    )

    init {
        setHistoryLiveData.value = history
    }

    fun onHistoryItemClicked(item: HistoryItem) {
        showToastLiveData.value = "Clicked id -> ${item.analyzeId}"
    }

    fun onBackClicked() {
        appRouter.exit()
    }
}