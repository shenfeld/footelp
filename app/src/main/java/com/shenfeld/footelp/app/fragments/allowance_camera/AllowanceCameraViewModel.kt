package com.shenfeld.footelp.app.fragments.allowance_camera

import com.shenfeld.footelp.base.BaseViewModel
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class AllowanceCameraViewModel : BaseViewModel() {
    fun onBackClicked() {
        appRouter.exit()
    }
}