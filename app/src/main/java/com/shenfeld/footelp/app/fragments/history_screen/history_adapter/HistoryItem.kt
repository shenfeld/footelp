package com.shenfeld.footelp.app.fragments.history_screen.history_adapter

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateLayoutContainer
import com.shenfeld.footelp.R
import com.shenfeld.footelp.base.extensions.DateExtensions
import com.shenfeld.footelp.base.extensions.formatWithPattern
import com.shenfeld.footelp.base.extensions.onClick
import kotlinx.android.synthetic.main.delegate_history_item.view.*
import org.threeten.bp.LocalDateTime

open class HistoryItem(
    val analyzeId: Int,
    val date: LocalDateTime,
    val status: Status,
) {
    enum class Status(val value: String) {
        HEALTHY("Healthy"),
        MINOR_DEFORMATION("Minor deformation"),
        DEFORMED("Deformed"),
        SIGNIFICANT_DEFORMATION("Significant deformation"),
        SEE_A_DOCTOR("See a doctor")
    }
}

fun historyAdapterDelegate(onItemClicked: (HistoryItem) -> Unit) =
    adapterDelegateLayoutContainer<HistoryItem, Any>(layout = R.layout.delegate_history_item) {
        itemView.onClick {
            onItemClicked.invoke(item)
        }

        bind {
            with(itemView) {
                tvDate.text = item.date.formatWithPattern(DateExtensions.ddMMyyyyHHmm)
                tvStatus.text = item.status.value

                if(item.status == HistoryItem.Status.SIGNIFICANT_DEFORMATION) {
                    tvStatus.setTextColor(getColor(R.color.pink_F272A5))
                }

                when(item.status) {
                    HistoryItem.Status.SIGNIFICANT_DEFORMATION -> tvStatus.setTextColor(getColor(R.color.pink_F272A5))
                    HistoryItem.Status.DEFORMED -> tvStatus.setTextColor(getColor(R.color.colorAccent))
                    else -> tvStatus.setTextColor(getColor(R.color.black_333333))
                }
            }
        }
    }